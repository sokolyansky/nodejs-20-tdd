'use strict';

const expect = require('chai').expect;
const Calculator = require('../src/calculator');

describe('String Calculator', () => {

  it('returns 0 if input is empty', () => {
    const calculator = new Calculator();

    const sum = calculator.add('');

    expect(sum).to.equal(0);
  });

  it('returns input number', () => {
    const calculator = new Calculator();

    const sum = calculator.add('2');

    expect(sum).to.equal(2);
  });

  it('returns sum of two comma separated numbers', () => {
    const calculator = new Calculator();

    const sum = calculator.add('1,2');

    expect(sum).to.equal(1 + 2);
  });

  it('returns sum any comma separated numbers', () => {
    const calculator = new Calculator();

    const sum = calculator.add('1,2,3,4,5');

    expect(sum).to.equal(1 + 2 + 3 + 4 + 5);
  });

  it('returns sum of two numbers separated by a new line', () => {
    const calculator = new Calculator();

    const sum = calculator.add('1\n2');

    expect(sum).to.equal(1 + 2);
  });

  it('returns sum of two numbers separated by any delimiter', () => {
    const calculator = new Calculator();

    const sum = calculator.add(';;\n1;;2');

    expect(sum).to.equal(1 + 2);
  });

  it('print console one negative number is not alowed', () => {
    const calculator = new Calculator();

    let msg;
    const log = console.log;
    console.log = function (message) {
      msg = message;
    };
    calculator.add('-55');
    console.log = log;

    expect(msg).to.equal('Negative numbers are not allowed -55');
  });

  it('print console two negative numbers are not allowed', () => {
    const calculator = new Calculator();

    let msg;
    const log = console.log;
    console.log = function (message) {
      msg = message;
    };
    calculator.add('-1,-2');
    console.log = log;

    expect(msg).to.equal('Negative numbers are not allowed -1,-2');
  });

  it('should ignore numbers more than 1000', () => {
    const calculater = new Calculator();

    const sum = calculater.add('2,1001');

    expect(sum).to.equal(2);
  });
});