class Calculator {
  constructor() {
    this.delimiter;
  }

  get defaultValue() {
    return 0;
  }

  add(input) {
    this.delimiter = /[,\n]/;

    if(this.inputIsEmpty(input) ) {
      return this.defaultValue;
    }

    if(this.isSingleNumber(input)) {
      return this.processSingleNumber(input);
    }

    if(this.isAnyDelimiter(input)) {
      return this.processAnyDelimiter(input);
    }

    return this.processMultiNumber(input);
  };


  inputIsEmpty(input) {
    return input.length === 0;
  }

  isSingleNumber(input) {
    return input.indexOf(',') === -1 && input.indexOf('\n') === -1;
  }

  processSingleNumber(input) {
    const res = parseInt(input);
    if(this.isNegative(res)) {
      this.printNegativeMessage(res);
      return;
    }

    if(res > 1000) {
      return 0;
    }

    return parseInt(input);
  }

  processMultiNumber(input) {
    /*const numbers = input.split(',');
    return numbers.reduce((sum, number) => this.processSingleNumber(sum) + this.processSingleNumber(number));*/

    const numbers = input
      .split(this.delimiter)
      .map((num) => {
        if(parseInt(num) > 1000) return 0;

        return parseInt(num);
      });

    const res = numbers.filter((num) => this.isNegative(num));
    if(res.length) {
      this.printNegativeMessage(res);
      return
    }

    return numbers.reduce((sum, number) => sum + number);
  }

  isAnyDelimiter(input) {
    let idx = input.indexOf('\n');

    return idx !== -1 && !isFinite(input[idx - 1]);
  }

  processAnyDelimiter(input) {
    let idx = input.indexOf('\n');

    const str = input.slice(idx + 1);
    this.delimiter = input.slice(0, idx);

    return this.processMultiNumber(str);
  }

  isNegative(input) {
    return input < 0;
  }

  printNegativeMessage(input) {
    console.log(`Negative numbers are not allowed ${input}`);
  }

}


module.exports = Calculator;